import { API_BASE_URL } from "../appState";
import { sendRequest } from "../../components/HelperComponents/functions";

export const login = (data, dispatch) => {
    sendRequest(`${API_BASE_URL}/login/`, "POST", data)
        .then(response => response.json())
        .then(data => {
                dispatch({
                    type: 'LOGIN',
                    payload: data
                });
        })
        .catch(error => console.log(error))
};