import { API_BASE_URL } from "../appState";
import { sendRequest } from "../../components/HelperComponents/functions";

export const postSubscribe = (data, dispatch) => {
    sendRequest(`${API_BASE_URL}/add-subscriber/`, "POST", data)
        .then(response => response.json())
        .then(data => {
            if (data.ok === 'False') {
                dispatch({
                    type: 'POST_SUBSCRIBE',
                    payload: 'error'
                });
            } else {
                dispatch({
                    type: 'POST_SUBSCRIBE',
                    payload: 'success'
                });
            }
        })
        .catch(error => console.log(error))
};