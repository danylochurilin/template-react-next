import React, { createContext, useReducer } from 'react';
import combineReducers from 'react-combine-reducers';

import { BaseReducer, initialBaseState } from './reducers/baseReducer';
import * as baseActions from './actions/baseActions';

import { AuthReducer, initialAuthState } from './reducers/authReducer';
import * as authActions from './actions/authActions';

export const API_BASE_URL = 'http://api.template.4-com.pro/api';

export const AppContext = createContext({
    ...initialAuthState,
    ...initialBaseState
});

export const AppProvider = ({ children }) => {
    const [rootReducerCombined, initialStateCombined] = combineReducers({
        BaseReducer: [BaseReducer, initialBaseState],
        AuthReducer: [AuthReducer, initialAuthState]
    });

    const [state, dispatch] = useReducer(rootReducerCombined, initialStateCombined);

    const allActions = {
        ...baseActions,
        ...authActions
    };
    console.log(state);
    return (
        <AppContext.Provider value={{
            subscription: state.BaseReducer.subscription,
            dispatch: dispatch,
            ...allActions
        }}>
            {children}
        </AppContext.Provider>
    );
};