export const initialBaseState = {
    subscription: ''
};

export const BaseReducer = (state, action) => {
    switch(action.type) {
        case 'POST_SUBSCRIBE':
            return {
                ...state,
                subscription: action.payload
            };

        default:
            return state;
    }
};