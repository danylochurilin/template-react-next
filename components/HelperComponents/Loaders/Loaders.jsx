import React from "react";
import { ImpulseSpinner, ClapSpinner } from "react-spinners-kit";
import TransitionedBlock from '../TransitionedBlock/TransitionedBlock';

import './Loaders.scss';


export class PageLoader extends React.Component {
    render() {
        return (
            <TransitionedBlock>
                <div className="container_block page_loader_wrapper">
                    <div className="page_loader ">
                        <ClapSpinner
                            size={50}
                            frontColor="#F93417"
                            // backColor="#686769"
                            sizeUnit="px"
                        />
                    </div>
                </div>
            </TransitionedBlock>
        );
    }
}

export class ButtonLoader extends React.Component {
    render() {
        return (
            <div className="button_loader">
                <ImpulseSpinner
                    size={30}
                    frontColor="#FFFFFF"
                    backColor="#733F24"
                    sizeUnit="px"
                />
            </div>
        );
    }
}