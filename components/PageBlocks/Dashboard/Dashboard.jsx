import React, {useContext, useEffect, useState} from "react";

import { AppContext } from "../../../context/appState";

const Dashboard = ({ dashboard }) => {
    const [currentPage, setCurrentPage] = useState(1);
    const { dispatch } = useContext(AppContext);

    useEffect(() => {
        console.log(dashboard);
    }, []);

    return (
        <div className="dashboard_wrapper">
            Template
        </div>
    )
};

export default Dashboard;