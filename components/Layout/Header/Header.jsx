import Link from 'next/link';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from "react";

// import favicon from "../../../public/images/favicon.ico";

import './Header.scss';

const Header = () => {

    const browserStorage = (typeof localStorage === 'undefined') ? [] : localStorage;

    const router = useRouter();

    return (
        <header className="header">
            <Head>
                <meta charSet="utf-8"/>
                {/*<link rel="shortcut icon" type="image/x-icon" href={favicon} />*/}
                <meta name="viewport" content="width=1300, user-scalable=yes" />
                <title>Template</title>
            </Head>
            <div className="content_block header_block">
               header
            </div>
        </header>
    )
};

export default Header;