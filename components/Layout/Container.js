import React, { useState, useEffect } from "react";
import Header from './Header/Header';
import Footer from './Footer/Footer';
import TransitionedBlock from "../HelperComponents/TransitionedBlock/TransitionedBlock";
import { AppProvider } from '../../context/appState';

import './styles/main.scss';

const Layout = props => {
    const [deal_shown, setShowingDeal] = useState(false);
    const [loading, setLoading] = useState(false);

    const browserStorage = (typeof localStorage === 'undefined') ? [] : localStorage;

    const onClose = () => {
        browserStorage.setItem('special_deal', "shown");
        setShowingDeal(true);
    };

    if (loading) return null;

    return (
        <div id="wrapper">
            <TransitionedBlock>
                <AppProvider>
                    <div className='relative_wrapper'>
                        <Header />
                        <div className="page_wrapper">
                            {props.children}
                        </div>
                        {/*<Footer />*/}
                    </div>
                </AppProvider>
            </TransitionedBlock>
        </div>
    );
};

export default Layout;