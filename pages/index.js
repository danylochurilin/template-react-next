import React from "react";

import { API_BASE_URL } from "../context/appState";

import Layout from "../components/Layout/Container";
import Dashboard from '../components/PageBlocks/Dashboard/Dashboard';

import "../components/PageBlocks/Dashboard/Dashboard.scss";

const Home = ({ dashboard }) => (
    <Layout>
        <Dashboard dashboard={dashboard} />
    </Layout>
);

// Home.getInitialProps = async () => {
//     const res = await fetch(`${API_BASE_URL}/dashboard`);
//     const json  = await res.json();
//     return {
//         dashboard: json
//     };
// };


export default Home;
